import jenkspy
from math import sqrt
import matplotlib.pyplot as plt
from multiprocessing import Pool
import numpy as np
import random
from scipy.stats import pearsonr
from statistics import mean, stdev
from statsmodels.stats.weightstats import ttest_ind
from statsmodels.stats.multitest import multipletests

from samesame import samesameBHtest


class Protein:
    def __init__(self):
        self.id = None
        self.spcs = []
        self.safs = []
        self.molw = 0.0
        self.is_differentially_regulated = False
        self.up_or_down_regulated = None
        self.is_false_positive = False
        self.pval = 0.0
        self.pearson_cc = 0.0
        self.cohens_d = 0.0
        self.protein_count = 0
        self.qval = 0.0


class Data:
    def __init__(
        self,
        total: int,
        replicates: int,
        states: int,
        regulation: float,
        false_positives: float,
        regulation_direction: float,
        molw_spread: str,
        count_range: list,
        count_spread: list,
        pearson_input: float,
        cohens_input: float,
        boxplot: bool,
        jenks_classes: int,
        bh_alpha: float,
    ):
        self.total = total
        self.replicates = replicates
        self.states = states
        self.differential_regulation = regulation
        self.differential_regulation_direction = regulation_direction
        self.false_positives = false_positives
        self.molw_spread = molw_spread
        self.count_range = count_range
        self.count_spread = count_spread
        self.pearson_limit = pearson_input
        self.cohens_limit = cohens_input
        self.jenks_classes = jenks_classes
        self.bh_alpha = bh_alpha

        self.protein_dict = {}
        self.jenks = []

        pool = Pool()
        proteins = pool.map(self.makeProteins, [i for i in range(self.total)])
        pool.close()
        pool.join()

        for p in proteins:
            self.protein_dict[p.id] = p

        proteins = None

        self.makeQValues()
        self._calculateJenks()
        if boxplot:
            self.makeBoxplot()

    def makeBoxplot(self):

        state_data = {}
        for state in range(self.states):

            replicate_data = {}
            for replicate in range(self.replicates):

                protein_data = []
                for pro in list(self.protein_dict.values()):

                    protein_data.append(pro.spcs[state][replicate])

                replicate_data[replicate] = protein_data

            state_data[state] = replicate_data

        fig, ax = plt.subplots(1, 2)
        for state in range(self.states):
            ax[state].boxplot(state_data[state].values())
            ax[state].set_xticklabels([i + 1 for i in state_data[state].keys()])
            ax[state].set_title(f"State {state + 1} Abundances for SpCs")
        plt.show()

    def makeQValues(self):
        qvals = multipletests(
            [pro.pval for pro in list(self.protein_dict.values())],
            alpha=self.bh_alpha,
            method="fdr_bh",
            is_sorted=False,
            returnsorted=False,
        )[1]
        for i, pro in enumerate(list(self.protein_dict.values())):
            pro.qval = qvals[i]

    def _calculateJenks(self):
        for state in range(self.states):
            j_states = []
            j_states_average = []
            for replicate in range(self.replicates):
                j_states.append(
                    jenkspy.jenks_breaks(
                        np.asarray([i.safs[state][replicate] for i in self.protein_dict.values()]),
                        nb_class=self.jenks_classes,
                    )
                )
            for c in range(self.jenks_classes):
                j_states_average.append(round(np.mean([i[c] for i in j_states]), 4))
            self.jenks.append(j_states_average)

    def _checkFalsePositive(self):
        num = random.randrange(0, 100)
        if 100 * self.false_positives <= num:
            return False
        else:
            return True

    def _checkProteinAbundance(self, spread):
        num = random.randrange(0, 100)
        b1 = (0, 100 * spread[0])
        b2 = (b1[1], b1[1] + (100 * spread[1]))
        b3 = (b2[1], b2[1] + (100 * spread[2]))
        b4 = (b3[1], b3[1] + (100 * spread[3]))
        b5 = (b4[1], b4[1] + (100 * spread[4]))
        if b1[0] <= num < b1[1]:
            return int(0)
        elif b2[0] <= num < b2[1]:
            return int(1)
        elif b3[0] <= num < b3[1]:
            return int(2)
        elif b4[0] <= num < b4[1]:
            return int(3)
        elif b5[0] <= num <= b5[1]:
            return int(4)

    def _checkDiffRegulation(self):
        num = random.randrange(0, 100)
        num2 = random.randrange(0, 100)
        if num <= 100 * self.differential_regulation:
            if num2 <= 100 * self.differential_regulation_direction:
                return True, "Up"
            else:
                return True, "Down"
        else:
            return False, None

    def _checkProteinMolw(self):

        """
        Based off Quantile Data from Iniga's C vs 34 Experiment
        These are AA weights -- (kDa/110)*1000 --, not kDa.
        Q0 --- 17
        Q25 -- 232
        Q50 -- 346
        Q75 -- 486
        Q100 - 2202
        """

        if self.molw_spread == "even":
            chance = random.randint(1, 4)
            if chance == 1:
                return round(random.uniform(17, 232), 3)
            elif chance == 2:
                return round(random.uniform(233, 346), 3)
            elif chance == 3:
                return round(random.uniform(347, 486), 3)
            else:
                return round(random.uniform(487, 2202), 3)
        elif self.molw_spread == "high":
            return round(random.uniform(500, 2200), 3)
        elif self.molw_spread == "low":
            return round(random.uniform(15, 250), 3)
        elif self.molw_spread == "tails":
            num = random.randrange(1, 3)
            if num == 1:
                return round(random.uniform(15, 250), 3)
            else:
                return round(random.uniform(500, 2200), 3)

    def makeProteins(self, input_id):
        """
        Dict of Proteins --> {}
        Each Protein --> {'pID1': [<Protein>, molw]}
        Every State --> [6,5,4,3,2,1]

        So protein_dict['pID1'][0].states[0][2]
        Corresponds to the 3rd replicate of state 1 from pID1
        """

        count = 0

        print(f"{input_id+1} of {self.total} proteins made.", end="\n", flush=True)

        protein = Protein()

        # Setting up the protein specifics
        protein_molw = self._checkProteinMolw()
        is_false_positive = self._checkFalsePositive()
        is_differentially_regulated, up_or_down_regulated = self._checkDiffRegulation()
        protein_abundance = self._checkProteinAbundance(self.count_spread)

        # Main loop for diff protein
        if is_differentially_regulated:
            pval = 0.5
            cohens_d = 0.1
            while True:
                count += 1

                protein.spcs = []
                protein.safs = []

                for s in range(self.states):

                    state_list_spcs = []
                    state_list_safs = []

                    for _ in range(self.replicates):

                        if s == 0:
                            num = random.randrange(
                                self.count_range[protein_abundance][0],
                                self.count_range[protein_abundance][1],
                            )
                            state_list_spcs.append(num)
                            state_list_safs.append(num / protein_molw)

                        elif s > 0:
                            if up_or_down_regulated == "Up":
                                b = protein_abundance
                                if b > 4:
                                    b = 4
                                num = random.randrange(
                                    self.count_range[b][0], self.count_range[b][1]
                                )
                            else:
                                b = protein_abundance
                                if b < 0:
                                    b = 0
                                num = random.randrange(
                                    self.count_range[b][0], self.count_range[b][1]
                                )

                            state_list_spcs.append(num)
                            state_list_safs.append(num / protein_molw)

                    protein.spcs.append(state_list_spcs)
                    protein.safs.append(state_list_safs)

                pval = ttest_ind(
                    protein.safs[0], protein.safs[1], alternative="two-sided", usevar="pooled"
                )[1]
                pearson, _ = pearsonr(protein.safs[0], protein.safs[1])
                cohens_d = (mean(protein.safs[0]) - mean(protein.safs[1])) / (
                    sqrt((stdev(protein.safs[0]) ** 2 + stdev(protein.safs[1]) ** 2) / 2)
                )

                if pval < 0.05 and abs(cohens_d) >= self.cohens_limit:
                    break
                else:
                    continue

        # Main loop for non-diff protien
        else:
            pval = 0.01
            cohens_d = 0.1
            while True:
                count += 1

                protein.spcs = []
                protein.safs = []

                for s in range(self.states):

                    state_list_spcs = []
                    state_list_safs = []

                    for _ in range(self.replicates):

                        num = random.randrange(
                            self.count_range[protein_abundance][0],
                            self.count_range[protein_abundance][1],
                        )
                        state_list_spcs.append(num)
                        state_list_safs.append(num / protein_molw)

                    protein.spcs.append(state_list_spcs)
                    protein.safs.append(state_list_safs)

                pval = ttest_ind(
                    protein.safs[0], protein.safs[1], alternative="two-sided", usevar="pooled"
                )[1]
                pearson, _ = pearsonr(protein.safs[0], protein.safs[1])
                try:
                    cohens_d = (mean(protein.safs[0]) - mean(protein.safs[1])) / (
                        sqrt((stdev(protein.safs[0]) ** 2 + stdev(protein.safs[1]) ** 2) / 2)
                    )
                except ZeroDivisionError:
                    cohens_d = 0

                if pval >= 0.05 and abs(cohens_d) >= self.cohens_limit:
                    break
                else:
                    continue

        # Collecting all of the protein information
        protein.id = str(input_id)
        protein.molw = protein_molw
        protein.is_differentially_regulated = is_differentially_regulated
        protein.up_or_down_regulated = up_or_down_regulated
        protein.is_false_positive = is_false_positive
        protein.pval = pval
        protein.pearson_cc = pearson
        protein.cohens_d = cohens_d
        protein.process_count = count

        return protein


if __name__ == "__main__":
    experiment = Data(
        total=1000,
        replicates=6,
        states=2,
        regulation=0.05,
        false_positives=0.005,
        regulation_direction=0.5,
        molw_spread="even",
        count_range=[(1, 4), (3, 7), (6, 50), (45, 70), (65, 150)],
        count_spread=[0.35, 0.3, 0.3, 0.045, 0.005],
        pearson_input=0.8,
        cohens_input=0.85,
        boxplot=False,
        jenks_classes=5,
        bh_alpha=0.1,
    )
    print(experiment.jenks)

    ss = samesameBHtest(
        experiment,
        experiment.states,
        experiment.false_positives,
        experiment.bh_alpha,
        )
    print(list(ss.values()))
