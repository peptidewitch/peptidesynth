from itertools import combinations
from functools import partial
import numpy as np
from multiprocessing import Pool
from statistics import mean
from statsmodels.stats.weightstats import ttest_ind
from statsmodels.stats.multitest import multipletests


def _conduct10tests(experiment, state, test_alpha, test):
    combo = [x for x in combinations([i for i in range(6)], 3)]
    comborev = combo[::-1]
    ss_dummy1 = []
    ss_dummy2 = []
    ttests = []
    bhtests = []
    bhcuttof_results = []
    for protein in experiment:
        ss_dummy1.append([x for i, x in enumerate(protein.safs[state]) if i in combo[test]])
        ss_dummy2.append([x for i, x in enumerate(protein.safs[state]) if i in comborev[test]])
    for t1, t2 in zip(ss_dummy1, ss_dummy2):
        ttests.append(
            ttest_ind(t1, t2, alternative="two-sided", usevar="unequal")[1]
        )

    ttests = np.asarray(ttests)
    ttests[np.isnan(ttests)] = 1

    bhtests.append(
        multipletests(
            ttests, alpha=test_alpha, method="fdr_bh", is_sorted=False, returnsorted=False
        )[1]
    )

    bhtests = bhtests[0]

    for x in range(1, 101):
        x = x / 100
        counter = 0
        for result in bhtests:
            if result < x:
                counter += 1
        differential_expression = counter / len(bhtests)
        bhcuttof_results.append(differential_expression)

    return bhcuttof_results


def samesameBHtest(experiment, states: int, desired_cutoff: float, test_alpha: float):

    state_results = {}

    for state in range(states):

        pool = Pool()
        func = partial(_conduct10tests, list(experiment.protein_dict.values()), state, test_alpha)
        bhcuttof_results_container = pool.map(func, [i for i in range(10)])
        pool.close()
        pool.join()

        # index same same method
        indexes = []
        for test in bhcuttof_results_container:
            for i, result in enumerate(test):
                if result < desired_cutoff:
                    continue
                else:
                    indexes.append(i)
                    break

        state_results[f"State{state}"] = mean(indexes)

    return state_results
